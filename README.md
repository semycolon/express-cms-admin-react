# Express CMS Admin Dashboard 

###Export project as express middleware
```js
const reactDashboardBaseUrl = '/dashboard/base/url';
const apiBaseUrl = 'http://domain.com/api/base/url';

const ExportReactAsMiddleware = require('@semycolon/express-cms-react')
const middleware = ExportReactAsMiddleware.generateMiddleware(reactDashboardBaseUrl, apiBaseUrl);
app.use(middleware);
```
