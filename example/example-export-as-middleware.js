const express = require('express');
const http = require('http');
const ExportReactAsMiddleware = require('../export');
const PORT = 1845
const app = express();


const reactDashboardBaseUrl = '/first/dashboard/base/url';
const apiBaseUrl = 'http://localhost:8085/cms/express/v1';
const reactDashboardBaseUrl2 = '/second/dashboard/base/url'

app.use(ExportReactAsMiddleware.generateMiddleware(reactDashboardBaseUrl, apiBaseUrl));
app.use(ExportReactAsMiddleware.generateMiddleware(reactDashboardBaseUrl2, apiBaseUrl));

const server = http.createServer(app);
server.listen(PORT, () => {
    console.log('server might be listening at http://localhost:' + PORT + reactDashboardBaseUrl,
        ' and ', 'http://localhost:' + PORT + reactDashboardBaseUrl2,);
})