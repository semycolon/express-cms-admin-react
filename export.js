const express = require('express');
const fs = require('fs');
const {join} = require('path');



module.exports.generateMiddleware = function (reactDashboardBaseUrl, apiBaseUrl) {
    if (!reactDashboardBaseUrl || !apiBaseUrl) {
        throw new Error('bad arguments,reactDashboardBaseUrl,apiBaseUrl should be defined!')
    }
    const BUILD_PATH = join(__dirname, 'build');
    if (!fs.existsSync(BUILD_PATH)) {
        throw new Error('build directory does not exists!, run \'yarn build\' to fix build project.')
    }

    const app = express();


    const fileExtensions = ['.txt', '.ico', '.js', '.css', '.png', '.jpg', '.jpeg','.gif',];
    const staticMiddleware = express.static(BUILD_PATH);
    const indexHtmlContent = getIndexHtmlContent();

    const htmlServerMiddleware = (req, res, next) => {
        res.send(indexHtmlContent);
    }

    app.use(reactDashboardBaseUrl,(req, res, next) => {
        const isAFile = fileExtensions.find(ext => req.url.endsWith(ext))
        if (isAFile) {
            staticMiddleware(req, res, next);
        } else {
            htmlServerMiddleware(req, res, next);
        }
    })

    return app;

    function getIndexHtmlContent() {
        let IndexPath = join(__dirname, 'build', 'index.html');
        if (!fs.existsSync(IndexPath)) {
            throw new Error(IndexPath+ 'does not exists, please make sure run \'yarn build\' successfully')
        }

        let indexHtmlContent = fs.readFileSync(IndexPath, {encoding: 'utf-8'});
        indexHtmlContent = indexHtmlContent.replace(/##baseurl_placeholder##/gi, reactDashboardBaseUrl)
        indexHtmlContent = insertBefore(indexHtmlContent, '<noscript>', `<script>window.__baseUrl__ = '${reactDashboardBaseUrl}'; window.__apiBaseUrl__ = '${apiBaseUrl}'</script>`)
        return indexHtmlContent;

    }
    function insertBefore(strToSearch, strToFind, strToInsert) {
        let n = strToSearch.lastIndexOf(strToFind);
        if (n < 0) return strToSearch;
        return strToSearch.substring(0, n) + strToInsert + strToSearch.substring(n);
    }
}

