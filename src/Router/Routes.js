import Dashboard from "../components/dashboard/Dasbhoard";
import Login from "../components/auth/login/Login";
import React from "react";
import {Link} from 'react-router-dom';
import NotFoundPage from "../components/404/404Page";
import Home from "../components/home/Home";
import SuperAdminRegister from "../components/auth/super-admin-register/superAdminRegister";

export const PageRoutes = [

    {
        name:'home',
        path:'/',
        component: Home,
        exact:true,
    },
    {
        name:'login',
        path:'/login',
        component: Login,
    },
    {
        name:'super-admin-register',
        path:'/super-admin-register',
        component: SuperAdminRegister,
    },
    {
        name:'dashboard',
        path:'/dashboard',
        component: Dashboard,
        auth:true,
    },
    {
        name:'404',
        path:'/*',
        component:NotFoundPage,
    }
]

