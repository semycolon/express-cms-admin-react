import React from "react";
import {Link} from "react-router-dom";

export default function NotFoundPage(props) {
    return (
        <div>
            <div>Page not found</div>
            <br/>
            <br/>
            <br/>
            <br/>
            <Link to="/" />
        </div>

    );
}