import React from 'react';
import {Route, Switch} from "react-router-dom";
import {PageRoutes} from "../Router/Routes";
import ProtectedRoute from "../lib/ProtectedRoute";
import WithAuth from "../lib/withAuth";
import Paper from "@material-ui/core/Paper";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {BrowserRouter as Router} from 'react-router-dom'
import {getBaseUrl} from "../lib/baseUrl";

const Styles = theme => ({
    paper: {
        minHeight: '100vh',
        borderRadius: 0,
    }
})
const useStyles = makeStyles(Styles)

function App() {
    const classes = useStyles();
    return (
        <Router basename={getBaseUrl()}>
            <Paper className={classes.paper}>
                <WithAuth>
                    <Switch>
                        {PageRoutes.map(route => ProtectedRoute(route))}
                    </Switch>
                </WithAuth>
            </Paper>
        </Router>
    );
}

export default App;
