import React, {Component,} from "react";
import Auth from "../../../lib/Auth";

import axios from 'axios';
import './login-classes.css';
import {Redirect} from "react-router-dom";
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';

import makeStyles from "@material-ui/core/styles/makeStyles";
import withStyles from "@material-ui/core/styles/withStyles";
import Avatar from "@material-ui/core/Avatar";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Button from "@material-ui/core/Button";
import Grid from "@material-ui/core/Grid";
import Link from "@material-ui/core/Link";
import CircularProgress from "@material-ui/core/CircularProgress";

class Login extends Component {
    state = {
        username: '',
        password: '',
        loading: false,
        errors: [],
        redirect: null,
    }

    handleLogin = async () => {
        try {
            this.beforeLogin();
            const {username, password} = this.state;
            const {data} = await axios.post('auth/login', {username, password})
            this.onUserLoggedIn(data);
            this.redirectToHome();
        } catch (e) {
            console.error(e);
            if (e.response && e.response.data && e.response.data.errors) {
                this.setState({
                    errors: e.response.data.errors,
                })
            }
        } finally {
            this.setState({
                loading: false,
            })
        }
    };

    beforeLogin() {
        this.setState({
            loading: true,
            errors: [],
        })
    }

    redirectToHome() {
        console.log('redirecting to home');
        const {history} = this.props;
        history.push('/');
    }

    render() {
        const {classes} = this.props;
        const {password, errors, loading, username} = this.state;
        const usernameErrors = errors.filter(it => it.param === 'username')
        const passwordErrors = errors.filter(it => it.param === 'password')
        return (
            <>
                {this.redirectIfRequired()}
                <Container component="main" maxWidth="xs">
                    <CssBaseline/>
                    <div className={classes.paper}>
                        <Avatar className={classes.avatar}>
                            <LockOutlinedIcon/>
                        </Avatar>
                        <Typography component="h1" variant="h5">
                            Sign In
                        </Typography>
                        <form className={classes.form} noValidate>
                            <TextField
                                value={username}
                                onChange={e => this.setState({username: e.target.value})}
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                name="username"
                                label="Username"
                                id="username"
                                autoComplete="username"
                                autoFocus
                                error={usernameErrors.length > 0}
                                helperText={usernameErrors ? usernameErrors.map(it => <div key={it.msg}>{it.msg}</div>) : null}
                            />
                            <TextField
                                value={password}
                                onChange={e => this.setState({password: e.target.value})}
                                variant="outlined"
                                margin="normal"
                                required
                                fullWidth
                                name="password"
                                label="Password"
                                id="password"
                                autoComplete="current-password"
                                type="password"
                                error={passwordErrors.length > 0}
                                helperText={passwordErrors ? passwordErrors.map(it => <div key={it.msg}>{it.msg}</div>) : null}
                            />

                            <FormControlLabel
                                control={<Checkbox value="remember" color={"primary"}/>}
                                label={"Remember me"}
                            />

                            <Button
                                onClick={(e) => {
                                    e.preventDefault();
                                    this.handleLogin()
                                }}
                                type={"submit"}
                                fullWidth
                                variant={"contained"}
                                color={"primary"}
                                className={classes.submit}
                            >
                                {
                                    loading
                                        ? <>Signing in <CircularProgress size={20} className={classes.progress}
                                                                         color="secondary"/> </>
                                        : 'Sign In'
                                }
                            </Button>

                            <Grid container>
                                <Grid item xs>
                                    <Link href="#" variant="body2">
                                        Forgot password?
                                    </Link>
                                </Grid>
                                <Grid item>
                                    <Link href="#" variant="body2">
                                        {"Don't have an account? Sign Up"}
                                    </Link>
                                </Grid>
                            </Grid>
                        </form>
                    </div>
                </Container>
            </>
            /*<div>

                Login Page
                <br/>
                <br/>
                <br/>
                <label>username</label>
                <br/>
                <input value={username} placeholder="username" onChange={(e) => {
                    this.setState({username: e.target.value})
                }}/>
                <br/>
                <div className="m-error">
                    {errors.filter(it => it.param === 'username').map(it => <div>{it.msg}</div>)}
                </div>
                <br/>
                <br/>
                <label>Password </label><br/>
                <input value={password} placeholder="password" onChange={(e) => {
                    this.setState({password: e.target.value})
                }}/>

                <div className="m-error">
                    {errors.filter(it => it.param === 'password').map(it => <div>{it.msg}</div>)}
                </div>
                <br/>
                <div>
                    <button onClick={this.handleLogin}>login</button>
                </div>

                <br/>
                {loading ? <div>Loading ...</div> : ''}
                </div>
    */
        );
    }

    redirectIfRequired = () => {
        const {redirect} = this.state
        return <>
            {Auth.isAuthenticated() ? <Redirect to="/"/> : ''}
            {redirect ? <Redirect to={redirect}/> : ''}
        </>;
    }

    onUserLoggedIn({user, token}) {
        Auth.setToken(token)
        Auth.setUser(user)
    }
}

const styles = theme => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    progress: {
        margin: theme.spacing(1),
    }
});

export default withStyles(styles)(Login);