import React from 'react';
import {Redirect} from "react-router-dom";
import Auth from "../../lib/Auth";

export default class Logout extends React.Component{
    state={
        redirect:null,
    }

    render() {
        const {redirect} = this.state;
        return (
            <div>
                {redirect ? <Redirect to="/login"/> : null}
                <button onClick={this.handleLogout} >Log Out</button>
            </div>
        );
    }

    handleLogout = () => {
        Auth.logout()
        this.setState({
            redirect:true,
        })
    }
}