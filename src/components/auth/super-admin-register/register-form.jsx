import React from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import CircularProgress from "@material-ui/core/CircularProgress";
import withStyles from "@material-ui/core/styles/withStyles";
import axios from "axios";
import Auth from "../../../lib/Auth";

class RegisterForm extends React.Component {
    state = {
        username: '',
        password: '',
        loading: false,
        errors: [],
    }

    render() {
        const {password, errors, loading, username} = this.state;
        const usernameErrors = errors.filter(it => it.param === 'username')
        const passwordErrors = errors.filter(it => it.param === 'password')
        const {classes} = this.props;

        return (
            <>
                <form className={classes.form}>
                    <TextField
                        value={username}
                        onChange={e => this.setState({username: e.target.value})}
                        variant="outlined"
                        margin="normal"
                        fullWidth
                        name="username"
                        label="Username"
                        id="username"
                        autoComplete="username"
                        autoFocus
                        error={usernameErrors.length > 0}
                        helperText={usernameErrors ? usernameErrors.map(it => <div key={it.msg}>{it.msg}</div>) : null}
                    />

                    <TextField
                        value={password}
                        onChange={e => this.setState({password: e.target.value})}
                        variant="outlined"
                        margin="normal"
                        fullWidth
                        name="password"
                        label="Password"
                        id="password"
                        autoComplete="current-password"
                        type="password"
                        error={passwordErrors.length > 0}
                        helperText={passwordErrors ? passwordErrors.map(it => <div key={it.msg}>{it.msg}</div>) : null}
                    />

                    <Button
                        onClick={(e) => {
                            e.preventDefault();
                            this.handleRegister()
                        }}
                        type={"submit"}
                        fullWidth
                        variant={"contained"}
                        color={"primary"}
                        className={classes.submit}
                    >
                        {
                            loading
                                ? <>Registering <CircularProgress size={20} className={classes.progress}
                                                                 color="secondary"/> </>
                                : 'Register'
                        }
                    </Button>
                </form>
            </>
        );
    }
    onUserLoggedIn({user, token}) {
        Auth.setToken(token)
        Auth.setUser(user)
    }



    handleRegister = async () => {
        try {
            this.setState({
                loading: true,
                errors: [],
            })
            const {extraParams,onSuccess} = this.props;
            const {username, password} = this.state;
            const {data} = await axios.post('auth/register', {username, password,...extraParams})
            this.onUserLoggedIn(data);
            onSuccess && typeof onSuccess === 'function' && onSuccess(data);
        } catch (e) {
            console.error(e);
            if (e.response && e.response.data && e.response.data.errors) {
                this.setState({
                    errors: e.response.data.errors,
                })
            }
        } finally {
            this.setState({
                loading: false,
            })
        }
    }
}
const Styles = theme => ({
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(1),
    },
})
export default withStyles(Styles)(RegisterForm);