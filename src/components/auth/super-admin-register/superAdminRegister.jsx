import React from "react";
import RegisterForm from './register-form';
import withStyles from "@material-ui/core/styles/withStyles";
import Card from "@material-ui/core/Card";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";

class SuperAdminRegister extends React.Component {
    render() {
        const {classes} = this.props;
        return (
            <>

                <Paper className={classes.main}>
                    <Card className={classes.mainCard}>
                        <Typography variant={"h4"} className={classes.cardHeader1}>
                            Super Admin
                        </Typography>
                        <Typography variant={"h4"} className={classes.cardHeader2}>
                            Registeration
                        </Typography>
                        <Typography variant={"body1"} className={classes.cardDesc}>
                            small description about super admin and make it not very long!
                        </Typography>
                        <RegisterForm extraParams={{superAdmin: true,}} onSuccess={this.redirectToHome}/>
                    </Card>
                </Paper>

            </>
        );
    }

    redirectToHome = () => {
        console.log('redirecting to home');
        const {history} = this.props;
        history.push('/');
    }
}

const Styles = theme => ({
    main: {
        display: 'flex',
        minHeight: '100vh',
        justifyContent: 'center',
        alignItems: 'center',
        // background:"red",
    },
    mainCard: {
        minHeight: 200,
        minWidth: 280,
        padding:theme.spacing(2),
    },
    cardHeader1: {
        textAlign: 'left',
    },
    cardHeader2: {
        textAlign: 'left',
        fontWeight: 300,
    },
    cardDesc: {
        fontWeight: 300,
        marginTop: theme.spacing(2),
    }
})

export default withStyles(Styles)(SuperAdminRegister);