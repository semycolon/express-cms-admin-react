import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import Button from "@material-ui/core/Button";
import DialogActions from "@material-ui/core/DialogActions";
import Dialog from '@material-ui/core/Dialog';
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";


class ConfirmationDialog extends React.Component {
    render() {
        const variant = this.props.variant || 'danger'
        const confirmationText = this.props.confirmationText || 'Confirm'
        const cancelText = this.props.cancelText || 'Cancel'
        const {onConfirm, onCancel,text,title,classes} = this.props;
        return (
            <>
                <Dialog open={true}>
                    {title && <DialogTitle id="alert-dialog-title">{title}</DialogTitle> }
                    {text && <DialogContent>
                        <DialogContentText id="alert-dialog-description">
                            {text}
                        </DialogContentText>
                    </DialogContent>
                    }
                    <DialogActions>
                        {variant === "danger" && (
                            <>
                                <Button className={classes.cancelBtn} onClick={(e) => onCancel()} autoFocus>
                                    {cancelText}
                                </Button>

                                <Button className={classes.confirmBtn} onClick={(e) => onConfirm()}>
                                    {confirmationText}
                                </Button>

                            </>
                        )}

                        {variant === "info" && (
                            <Button color="primary" onClick={(e) => onConfirm()}>
                                {confirmationText}
                            </Button>
                        )}
                    </DialogActions>
                </Dialog>
            </>
        );
    }
}

const Styles = theme => ({
    cancelBtn:{
        color:theme.palette.info.main,
    },
    confirmBtn:{
        color:theme.palette.error.main,
    },
});
export default withStyles(Styles)(ConfirmationDialog);