import React, {Component} from "react";
import Typography from "@material-ui/core/Typography";
import AddNewItem from "./add-new-item/add-new-item";
import DataTable from "./data-table/data-table";
import axios from 'axios';
import {connect} from 'react-redux';
import {ACTION_FETCH_CMS} from "../../../../redux/actions/types";

class CollectionDetails extends Component {
    state = {
        dataList: null,
    }

    render() {
        const {custom_type} = this.props;
        const {dataList} = this.state;
        if (!custom_type) {
            return null;
        }
        this.fetchDataList()
        return (
            <div>
                <Typography variant={"h4"}>{custom_type.name}</Typography>
                <DataTable custom_type={custom_type} dataList={this.props.cms[custom_type.name] || []}/>
                <AddNewItem custom_type={custom_type} />
            </div>
        );
    }

    fetchDataList = () => {
        const {custom_type} = this.props;
        const {dataList, loading} = this.state;

        if (!dataList && !loading) {
            this.setState({
                loading: true,
            })
            axios.get('/cms/list/' + custom_type.name)
                .then(res => {
                    console.log('res', res);
                    this.props.dispatch({
                        type: ACTION_FETCH_CMS, payload: {
                            collection_name: custom_type.name,
                            data: res.data,
                        }
                    })
                    this.setState({
                        dataList: [],
                    })
                })
                .catch(e => {
                    console.error(e);
                })
                .finally(() => this.setState({loading: false}))
        }
    }
}
const mapStateToProps = state => ({
    cms: state.cms.cms,
})
export default connect(mapStateToProps)(CollectionDetails);
