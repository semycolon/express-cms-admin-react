import React from 'react';
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import axios from "axios";
import Grid from "@material-ui/core/Grid";
import {connect} from 'react-redux';
import {ACTION_CMS_ADDED_ITEM} from "../../../../../redux/actions/types";
import Typography from "@material-ui/core/Typography";
import withStyles from "@material-ui/core/styles/withStyles";

class AddNewItem extends React.Component {
    state = {
        name: '',
        label: '',
        errors: [],
    }

    resetState = () => {
        const state = {...this.state};
        for (let k in state) {
            if (typeof state[k] === "string") {
                state[k] = '';
            } else if (Array.isArray(state[k])) {
                state[k] = [];
            }
        }

        this.setState(state);
    }

    render() {
        const {errors,} = this.state;
        const {custom_type,classes} = this.props;

        return (
            <div>
                <br/><br/>
                <Typography variant={"h4"}>Add New Item</Typography>
                <Grid container direction={"row"} justify={"flex-start"}>
                    {custom_type.fields.map(field => (
                        <Grid className={classes.new_grid_item} key={field.name} item xs={12} sm={6} md={4}>
                            {this.getInputForField(field)}
                        </Grid>
                    ))}
                </Grid>
                <br/><br/>
                <Button onClick={() => this.handleSubmit()} variant={"contained"} color={"primary"}>Add Field</Button>
            </div>
        );
    }

    handleSubmit = async () => {
        const {custom_type, onNewItem} = this.props;
        const {errors, ...rest} = this.state;

        try {
            const {data} = await axios.post(`/cms/add/${custom_type.name}`, rest)
            this.props.dispatch({
                type: ACTION_CMS_ADDED_ITEM, payload: {
                    collection_name: custom_type.name,
                    data,
                }
            })
            this.resetState();
        } catch (e) {
            console.error(e)
            if (e.response && e.response.data && e.response.data.errors) {
                this.setState({
                    errors: e.response.data.errors,
                })
            }
        } finally {
        }

    }

    getInputForField = field => {
        const {errors} = this.state;
        switch (field.type_name) {
            case "String": {
                return <>
                    <TextField
                        fullWidth
                        key={field.name}
                        label={field.label}
                        value={this.state[field.name] || ''}
                        onChange={(e) => this.setState({[field.name]: e.target.value,})}
                        error={errors.filter(it => it.param === field.name).length > 0}
                        helperText={errors.filter(it => it.param === field.name) ? errors.filter(it => it.param === field.name).map(it =>
                            <span>{it.msg ? it.msg : 'error'}</span>) : null}
                    />
                </>
            }
            case "Number" : {
                return <>
                    <TextField
                        fullWidth
                        type={"number"}
                        key={field.name}
                        label={field.label}
                        value={this.state[field.name] || ''}
                        onChange={(e) => this.setState({[field.name]: e.target.value,})}
                        error={errors.filter(it => it.param === field.name).length > 0}
                        helperText={errors.filter(it => it.param === field.name) ? errors.filter(it => it.param === field.name).map(it =>
                            <span>{it.msg ? it.msg : 'error'}</span>) : null}
                    />
                </>
            }
        }
    }
}
const Style = theme => ({
    new_grid_item:{
        margin:theme.spacing(2),
    }
})
const Styled = withStyles(Style)(AddNewItem);
export default connect()(Styled);