import React from 'react';
import withStyles from "@material-ui/core/styles/withStyles";
import TableContainer from "@material-ui/core/TableContainer";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import Paper from "@material-ui/core/Paper";

class DataTable extends React.Component {
    render() {
        const {custom_type: {fields}, classes,dataList} = this.props;
        return (
            <>
                <TableContainer component={Paper}>
                    <Table className={classes.table} aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                {fields.map(field =>
                                    <>
                                        <TableCell key={field.name}>{field.label}</TableCell>
                                    </>
                                )}
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {dataList.map((d, i) => <>
                                <TableRow key={i}>
                                    {fields.map((row) => (
                                        <TableCell>{d[row.name]}</TableCell>
                                    ))}
                                </TableRow>
                            </>)}
                        </TableBody>
                    </Table>
                </TableContainer>
            </>
        );
    }
}

const Styles = {
    table: {
        minWidth: 650,
    },
}
export default withStyles(Styles)(DataTable)