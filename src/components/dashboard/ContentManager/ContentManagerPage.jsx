import React from 'react';
import axios from 'axios';
import Grid from "@material-ui/core/Grid";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import {BrowserRouter as Router, Link, Switch, Route} from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import withStyles from "@material-ui/core/styles/withStyles";
import CollectionDetails from "./CollectionDetails/CollectionDetails";
import {connect} from 'react-redux';
import {fetchCollections} from "../../../redux/actions/collectionActions";
import {getBaseUrl} from "../../../lib/baseUrl";

class ContentManagerPage extends React.Component {
    state = {
        customTypes: [],
        loading: false,
    }

    componentDidMount() {
        this.props.fetchCollections();
    }

    render() {
        const {classes} = this.props;
        const {collections: customTypes} = this.props;
        return (
            <div>
                <Router>
                    <Grid container>
                        <Grid item xs={2}>
                            <List component="nav" aria-label="secondary mailbox folders">
                                {customTypes.filter(it => it._id).map(it =>
                                    <Link className={classes.link} key={it._id}
                                          to={getBaseUrl() + "/dashboard/content-manager/" + it._id}>
                                        <ListItem button>
                                            <Typography variant={"button"}>
                                                {it.name}
                                            </Typography>
                                        </ListItem>
                                    </Link>
                                )}

                            </List>
                        </Grid>
                        <Grid item xs={10}>
                            <Switch>
                                <Route path={getBaseUrl() + "/dashboard/content-manager/:_id"}
                                       render={(props) => <CollectionDetails key={props.match.params._id}
                                                                             custom_type={customTypes.find(it => it._id === props.match.params._id)} customTypes={customTypes} {...props} />}
                                />
                            </Switch>

                        </Grid>
                    </Grid>
                </Router>
            </div>
        );
    };
};

const Styles = (theme) => ({
    link: {
        textDecoration: "none",
        color: theme.palette.text.primary,
    }
})

let Styled = withStyles(Styles)(ContentManagerPage);
const mapStateToProps = state => ({
    collections: state.collections.collections,
})
export default connect(mapStateToProps, {fetchCollections})(Styled);