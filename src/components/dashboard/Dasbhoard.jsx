import * as React from "react";
import Auth from "../../lib/Auth";
import {BrowserRouter as Router, Link, NavLink, Redirect, Switch} from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import {Route} from "react-router-dom";
import Button from "@material-ui/core/Button";
import ContentTypeBuilderPage from "./content-type-builder/content-type-builder-page";
import withStyles from "@material-ui/core/styles/withStyles";
import CMS from "./ContentManager/ContentManagerPage";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Grid from "@material-ui/core/Grid";
import StorageIcon from '@material-ui/icons/Storage';
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import ContentBuilderIcon from '@material-ui/icons/Build';
import ContentManagementIcon from '@material-ui/icons/Inbox';
import LogoutIcon from '@material-ui/icons/ExitToApp';
import {getBaseUrl} from "../../lib/baseUrl";

const linkContentTypeBuilder = getBaseUrl()+"/dashboard/content-type-builder";
const linkContentManager = getBaseUrl()+"/dashboard/content-manager";

class Dashboard extends React.Component {
    state = {
        redirect: null,
    };

    render() {
        const {classes, location} = this.props;

        return (
            <div className="dashboard-root">
                <Router>
                    <Grid container className={classes.rootGrid}>
                        <Grid item xs={2} className={classes.col_left}>
                            <div className={classes.main}>
                                <StorageIcon className={classes.icon_main}/>
                                <Typography
                                    variant={"h5"}>
                                    <span className={classes.main_section}>Express </span>
                                    CMS
                                </Typography>
                            </div>
                            <List key={location.pathname}>
                                <Route  render={props => (
                                    <>
                                        {this.getNavLink(linkContentTypeBuilder, 'Content Type Builder',
                                            <ContentBuilderIcon/>, classes, props)}
                                        {this.getNavLink(linkContentManager, 'Content Manager',
                                            <ContentManagementIcon/>, classes, props)}
                                    </>
                                )}/>
                            </List>
                            <div className={classes.col_left_bottom}>
                                <Button onClick={this.handleLogout} fullWidth className={classes.logoutBtn}>
                                    <LogoutIcon className={classes.logoutIcon}/>Log Out
                                </Button>
                            </div>
                        </Grid>
                        <Grid item style={{flexGrow: 1}}>
                            <Switch>
                                <Route path={linkContentTypeBuilder} component={ContentTypeBuilderPage}/>
                                <Route path={linkContentManager} component={CMS}/>
                            </Switch>
                        </Grid>
                    </Grid>
                </Router>
                {this.state.redirect ? <Redirect to={this.state.redirect}/> : ''}
            </div>
        );
    }

    getNavLink(link, text, icon, classes, {location},) {
        return (
            <NavLink className={classes.link} to={link}>
                <ListItem button selected={location.pathname.startsWith(link)}>
                    <ListItemIcon>{icon}</ListItemIcon>
                    <ListItemText primary={text}/>
                </ListItem>
            </NavLink>
        );
    }

    handleLogout = () => {
        Auth.logout()
        this.setState({
            redirect: '/'
        })
    }
}

const Styles = (theme) => ({
    main: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: theme.spacing(4),
        paddingBottom: theme.spacing(4),
        background: theme.palette.secondary.main,
        whiteSpace: 'nowrap'
    },
    main_section: {
        color: theme.palette.text.secondary,
    },
    link: {
        textDecoration: "none",
        color: theme.palette.text.primary,
    },
    icon_main: {
        marginRight: theme.spacing(1),
        fontSize: '38px',
    },
    col_left: {
        maxWidth: '250px',
        minWidth: '200px',
        minHeight: '100vh',
        position: 'relative',
    },
    active_link: {
        background: 'red',
        cursor: "not-allowed",
    },
    col_left_bottom: {
        position: 'absolute',
        bottom: 0,
        width: '100%',
        marginBottom: theme.spacing(1),
    },
    logoutBtn: {
        color: theme.palette.error.main,
    },
    logoutIcon: {
        marginRight: theme.spacing(1),
    },
    rootGrid:{
        flexWrap:'nowrap',
    }
})

export default withStyles(Styles)(Dashboard);