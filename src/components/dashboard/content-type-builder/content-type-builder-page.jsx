import React from 'react';
import axios from 'axios';
import NewCustomType from "./content-type-details/new-content-type/new-content-type";

import Grid from "@material-ui/core/Grid";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";

import {BrowserRouter as Router, Link, Switch, Route} from "react-router-dom";
import Typography from "@material-ui/core/Typography";
import withStyles from "@material-ui/core/styles/withStyles";
import ContentTypeDetails from "./content-type-details/content-type-details";
import {connect} from 'react-redux';
import {fetchCollections} from "../../../redux/actions/collectionActions";
import {getBaseUrl} from "../../../lib/baseUrl";

class ContentTypeBuilderPage extends React.Component {
    state = {
        customTypes: [],
        loading: false,
    }

    componentDidMount() {
        this.props.fetchCollections();
    }

    render() {
        const {classes} = this.props;
        const {collections: contentType, location} = this.props;
        let newContentTypeLink = getBaseUrl() + "/dashboard/content-type-builder/new";
        return (
            <div>
                <Router>
                    <Grid container style={{flexWrap: 'nowrap'}}>
                        <Grid item xs={2} className={classes.row_left}>
                            <Route path={getBaseUrl() + "/dashboard/content-type-builder/"} render={props => {
                                return (
                                    <List component="nav" aria-label="secondary mailbox folders">
                                        {
                                            contentType && Array.isArray(contentType) && contentType.filter(it => it._id)
                                                .map(it => this.getListItem(it, classes, props))
                                        }

                                        <Link className={classes.link} to={newContentTypeLink}>
                                            <ListItem button selected={props.location.pathname === newContentTypeLink}>
                                                <Typography variant={"button"}>
                                                    New Custom Type
                                                </Typography>
                                            </ListItem>
                                        </Link>
                                    </List>
                                );
                            }}/>
                        </Grid>
                        <Grid item style={{flexGrow: 1}}>
                            <Switch>
                                <Route path={getBaseUrl() + '/dashboard/content-type-builder/new'}
                                       render={(props) => <NewCustomType {...props} />}
                                />

                                <Route path={getBaseUrl() + "/dashboard/content-type-builder/:_id"}
                                       render={(props) => <ContentTypeDetails key={props.match.params._id}
                                                                              custom_type={contentType.find(it => it._id === props.match.params._id)} {...props} />}
                                />
                            </Switch>

                        </Grid>
                    </Grid>
                </Router>
            </div>
        );
    };

    getListItem(contentType, classes, {location}) {
        const link = getBaseUrl() + "/dashboard/content-type-builder/" + contentType._id;
        return (
            <>
                <Link className={classes.link} key={contentType._id}
                      to={link}>
                    <ListItem button selected={location.pathname.startsWith(link)}>
                        <Typography variant={"button"}>
                            {contentType.name}
                        </Typography>
                    </ListItem>
                </Link>
            </>
        )
    }
};

const Styles = (theme) => ({
    link: {
        textDecoration: "none",
        color: theme.palette.text.primary,
    },
    row_left: {
        maxWidth: '250px', minWidth: '200px',
    }
})

const CustomModelsPageWithStyle = withStyles(Styles)(ContentTypeBuilderPage);

const mapStateToProps = state => ({
    collections: state.collections.collections,
})

export default connect(mapStateToProps, {fetchCollections})(CustomModelsPageWithStyle)