import withStyles from "@material-ui/core/styles/withStyles";
import React from "react";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import axios from 'axios';
import {COLLECTION_NEW_FIELD} from "../../../../../redux/actions/types";
import {connect} from 'react-redux';
import SelectField from './add-field';
import Typography from "@material-ui/core/Typography";

class AddFieldHolder extends React.Component {
    render() {
        const {custom_type} = this.props;

        return (
            <div>
                <br/>
                <br/>
                <br/>
                <Typography variant={"h2"}>Add New Field </Typography>
                <br/>
                <br/>
                <SelectField custom_type={custom_type} />
            </div>
        );
    }

}

const Styles = {}

let _withStyle = withStyles(Styles)(AddFieldHolder);
export default connect()(_withStyle)