import withStyles from "@material-ui/core/styles/withStyles";
import React from 'react';
import FieldsDefinition from './fields-definition';
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import FormHelperText from "@material-ui/core/FormHelperText";
import Button from "@material-ui/core/Button";
import axios from "axios";
import {COLLECTION_NEW_FIELD} from "../../../../../redux/actions/types";
import {connect} from 'react-redux';

class AddField extends React.Component {
    state = {
        selectedField: null,
        requestBody: {},
        errors:[],
    }
    handleCancel = () => {
        this.setState({
            selectedField: null,
            requestBody: {},
        })
    };

    render() {
        const {classes, custom_type,} = this.props;
        const {selectedField, requestBody,errors} = this.state;

        return (
            <>
                <div className={classes.root}>

                    {!selectedField && <>
                        <Typography variant={"h4"}>Select Field Type</Typography>
                        <Grid container>
                            {FieldsDefinition.map(f => <>
                                <Card key={f.field_type} onClick={(e) => this.handleFieldClicked(f)}
                                      className={classes.row_card}>
                                    <span className={classes.insideCard}><f.icon/></span>
                                    <Typography variant={"h6"}>{f.label}</Typography>
                                </Card>
                            </>)
                            }
                        </Grid>
                    </>}
                    {selectedField && <>
                        <div style={{display: 'flex'}}>
                            <selectedField.icon fontSize={"large"}/>
                            <Typography className={classes.selecteHeaderTitle}
                                        variant={"h4"}>{selectedField.label}</Typography>
                            <Button onClick={this.handleCancel} className={classes.cancelBtn}
                                    variant={"outlined"}>Cancel</Button>
                        </div>
                        <Typography variant={"h6"}>Basic Settings</Typography>
                        <Grid container spacing={2}>
                            {selectedField.base_settings.map(it => (
                                <Grid key={it.name} item xs={12} sm={6} md={4}>
                                    {this.getFieldByInput(it)}
                                </Grid>
                            ))}
                        </Grid>
                        <br/>
                        <Typography variant={"h6"}>Advanced Settings</Typography>
                        <Grid container spacing={2}>
                            {selectedField.advanced_settings.map(it => (
                                <Grid key={it.name} item xs={12} sm={6} md={4}>
                                    {this.getFieldByInput(it)}
                                </Grid>
                            ))}
                        </Grid>
                        <br/>
                        <Button onClick={this.handleSubmit} color={"primary"} variant={"contained"}>Add Field</Button>
                    </>}
                </div>
            </>
        );
    }

    handleFieldClicked = (field) => {
        this.setState({
            selectedField: field,
        })
    }

    handleSubmit = async () => {
        const {custom_type} = this.props;
        const {requestBody, selectedField} = this.state;
        const {field_type} = selectedField;
        try {
            const {data} = await axios.post(`/custom-types/${custom_type.name}`, {...requestBody, field_type})
            this.props.dispatch({type: COLLECTION_NEW_FIELD, payload: data})
            this.setState({
                errors: [],
                selectedField: null,
                requestBody: {},
            })
        } catch (e) {
            console.error(e)
            if (e.response && e.response.data && e.response.data.errors) {
                this.setState({
                    errors: e.response.data.errors,
                })
            }
        } finally {

        }
    }

    getFieldByInput = (it) => {
        const {requestBody, errors,} = this.state;
        const itemErrors = errors.filter(e => e.param === it.name);

        switch (it.type) {
            case 'text':
                return <TextField onChange={(e) => this.handleInputChange(it.type, e)}
                                  value={requestBody[it.name]}
                                  name={it.name}
                                  label={it.label || it.name}
                                  error={itemErrors.length > 0}
                                  helperText={itemErrors.length ? itemErrors.map(it =>
                                      <div>{it.msg ? it.msg : 'error'}</div>) : it.desc}
                />;
            case 'number':
                return <TextField onChange={(e) => this.handleInputChange(it.type, e)}
                                  value={requestBody[it.name]}
                                  name={it.name}
                                  label={it.label || it.name}
                                  type={'number'}
                                  error={itemErrors.length > 0}
                                  helperText={itemErrors.length ? itemErrors.map(it =>
                                      <div>{it.msg ? it.msg : 'error'}</div>) : it.desc}
                />;
            case 'boolean':
                return <>
                    <FormControlLabel
                        control={
                            <Checkbox
                                name={it.name}
                                onChange={(e) => this.handleInputChange(it.type, e)}
                                color={"primary"}
                            />
                        }
                        label={it.label || it.name}
                    />
                    <div>{itemErrors.length ? itemErrors.map(it =>
                        <FormHelperText error={true}>{it.msg ? it.msg : 'error'}</FormHelperText>) : <FormHelperText>{it.desc}</FormHelperText>}
                    </div>
                </>
            default:
                return <div>unknown field type , {it.type}</div>
        }
    }

    handleInputChange = (type, event) => {
        let name = event.target.name;
        let value;
        switch (type) {
            case 'text':
            case 'number':
                value = event.target.value;
                break;
            case 'boolean':
                value = event.target.checked;
                break;
        }
        const {requestBody} = this.state;
        this.setState({
            requestBody: {
                ...requestBody,
                [name]: value
            }
        })
    };
}

const Styles = theme => ({
    row_card: {
        minWidth: 120,
        margin: theme.spacing(1),
        padding: theme.spacing(1),
        display: 'flex',
        cursor: 'pointer',
    },
    insideCard: {
        margin: 'auto 0',
        marginRight: theme.spacing(1),
    },
    cancelBtn: {
        margin: theme.spacing(1),
        marginLeft: 'auto',
    },
    selecteHeaderTitle: {
        marginLeft: theme.spacing(1),
    },
    root: {
        margin: theme.spacing(1),
    }
})
const Styled = withStyles(Styles)(AddField)
export default connect()(Styled);