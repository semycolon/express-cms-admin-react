import TextFieldsIcon from '@material-ui/icons/TextFields';
import NumberFieldIcon from '@material-ui/icons/Filter9Plus';
export default [
    {
        label:'Text',
        field_type: 'text',
        icon:TextFieldsIcon,
        base_settings: [
            {
                name: 'name',
                type: 'text',
                desc: 'name of the field in the collection'
            },
            {
                label: 'Display Name',
                name: 'label',
                type: 'text',
                desc: 'display name in the dashboard',
            },
        ],
        advanced_settings: [
            {
                label: 'Default value',
                name: 'default',
                type: 'text',
                desc: 'default value of the field'
            },
            {
                name: 'required',
                type: 'boolean',
                desc: 'make this field mandatory to be Entered',
            },
            {
                name: 'private',
                type: 'boolean',
                desc: 'hide this field in Public APIs',
            },
            {
                name: 'unique',
                type: 'boolean',
                desc: 'prevent duplicate',
            },
            {
                name: 'index',
                type: 'boolean',
                desc: 'create index for this field, makes queries based on this field faster',
            },
            {
                name: 'lowercase',
                type: 'boolean',
                desc: 'convert value to lowercase',
            },
            {
                name: 'uppercase',
                type: 'boolean',
                desc: 'convert value to uppercase',
            },
            {
                name: 'trim',
                type: 'boolean',
                desc: 'remove white space at the beginning and end of value',
            },
        ],
    },
    {
        label: 'Number',
        field_type: 'number',
        icon: NumberFieldIcon,
        base_settings: [
            {
                name: 'name',
                type: 'text',
                desc: 'name of the field in the collection'
            },
            {
                label: 'Display Name',
                name: 'label',
                type: 'text',
                desc: 'display name in the dashboard',
            },
        ],
        advanced_settings: [
            {
                label: 'Default value',
                name: 'default',
                type: 'text',
                desc: ''
            },
            {
                name: 'required',
                type: 'boolean',
                desc: 'make this field mandatory to be Entered',
            },
            {
                name: 'private',
                type: 'boolean',
                desc: 'hide this field in Public APIs',
            },
            {
                name: 'unique',
                type: 'boolean',
                desc: 'prevent duplicate',
            },
            {
                name: 'index',
                type: 'boolean',
                desc: 'create index for this field, makes queries based on this field faster',
            },
            {
                label:'Minimum',
                name: 'min',
                type: 'number',
                desc: "don't accent values less than minimum",
            },
            {
                label:'Maximum',
                name: 'max',
                type: 'number',
                desc: "don't accent values greater than maximum",
            },
        ],
    },
]