import React, {Component} from "react";
import Typography from "@material-ui/core/Typography";
import AddNewField from "./add-field/add-field-holder";
import FieldsTable from "./fields-table/fields-table";
import DeleteCustomType from './delete-custom-type'
import withStyles from "@material-ui/core/styles/withStyles";

class ContentTypeDetails extends Component {
    state = {
        loading: false,
    }

    componentDidMount() {
        console.log('componentDidMount');
    }

    render() {
        const {custom_type, classes} = this.props;
        if (!custom_type) {
            return null;
        }
        return (
            <div>
                <div className={classes.head_row}>
                    <Typography variant="h2">{custom_type.name}</Typography>
                    <Typography variant={"h6"} className={classes.header_sub}>Collection</Typography>
                    <DeleteCustomType  custom_type={custom_type}/>
                </div>
                <div>
                    <FieldsTable custom_type={custom_type}/>
                </div>
                <div>
                    <AddNewField custom_type={custom_type}/>
                </div>

            </div>
        );
    }
}

const Styles = theme => ({
    head_row: {
        display: 'flex',
        paddingLeft: theme.spacing(4),
        paddingTop: theme.spacing(2),
    },
    header_sub: {
        margin: 'auto 0',
        paddingLeft: theme.spacing(1),
        fontWeight:300,
        marginRight:'auto',
    },

});

export default withStyles(Styles)(ContentTypeDetails)