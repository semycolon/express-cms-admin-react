import React, {Component} from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import {connect} from 'react-redux';
import Button from "@material-ui/core/Button";
import ConfirmationDialog from "../../../../common/confirmation-dialog";
import axios from "axios";
import {REMOVE_COLLECTION} from "../../../../../redux/actions/types";

class DeleteCustomType extends Component {
    state = {
        showConfirmDialog: false,
    }
    handleConfirm = async () => {
        this.hideDialog()
        const {custom_type} = this.props;
        try {
            const {data} = await axios.delete('/custom-types/' + custom_type.name);
            this.props.dispatch({type:REMOVE_COLLECTION,payload:data})
        } catch (e) {
            console.error(e)
        } finally {
        }
    };


    hideDialog = () => {
        this.setState({
            showConfirmDialog: false,
        })
    }

    onDeleteClicked = () => {
        this.showDialog();
    };

    showDialog = () => {
        this.setState({
            showConfirmDialog: true,
        })
    }

    render() {
        const {showConfirmDialog} = this.state;
        const {classes, custom_type} = this.props;
        return (
            <>
                <div className={classes.main}>
                    <Button onClick={() => this.showDialog()} variant={"outlined"}>Delete Collection</Button>
                </div>
                {
                    showConfirmDialog &&
                    <ConfirmationDialog title={`Delete Collection ${custom_type.name}?`}
                                        text={"all your data will be lost!, are you sure?"}
                                        onConfirm={() => this.handleConfirm()} onCancel={() => this.hideDialog()}/>
                }
            </>
        );
    }
}

const Styles = theme => ({
    main: {
        margin: theme.spacing(3),
    }
})
let DeleteWithStyles = withStyles(Styles)(DeleteCustomType);

export default connect()(DeleteWithStyles);