import React from 'react';
import withStyles from "@material-ui/core/styles/withStyles";
import TableContainer from "@material-ui/core/TableContainer";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import DeleteIcon from '@material-ui/icons/Delete';
import IconButton from "@material-ui/core/IconButton";
import ConfirmationDialog from "../../../../common/confirmation-dialog";
import axios from "axios";
import {ACTION_REMOVE_FIELD, REMOVE_COLLECTION} from "../../../../../redux/actions/types";
import {connect} from 'react-redux';

class FieldsTable extends React.Component {

    state = {
        showConfirmDialog: false,
        deleteItem: null,
    }

    render() {
        const {showConfirmDialog, deleteItem} = this.state;
        const {custom_type, classes,} = this.props;
        const {fields} = custom_type;
        if (!fields.length) {
            return (
                <div className={classes.emptyMain}>
                    <Typography variant={"body1"}>No Field yet !!!</Typography>
                </div>
            );
        }
        return (
            <>
                <TableContainer component={Paper}>
                    <Table className={classes.table} aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell>Label</TableCell>
                                <TableCell align="left">Name</TableCell>
                                <TableCell>Actions</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {fields.map((row) => (
                                <TableRow key={row.name}>
                                    <TableCell align="left">{row.label}</TableCell>
                                    <TableCell component="th" scope="row">
                                        {row.name}
                                    </TableCell>
                                    <TableCell>
                                        <IconButton>
                                            <DeleteIcon onClick={() => this.onDeleteFieldClicked(row)}/>
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                {
                    showConfirmDialog && deleteItem &&
                    <ConfirmationDialog title={`Delete Field ${deleteItem.name}?`}
                                        text={"are you sure?"}
                                        onConfirm={() => this.handleConfirm()} onCancel={() => this.hideDialog()}/>
                }
            </>
        );
    }

    onDeleteFieldClicked = (row) => {
        this.setState({
            deleteItem: row,
            showConfirmDialog: true,
        })
    }

    handleConfirm = async () => {
        this.hideDialog()
        const {deleteItem} = this.state;
        const {custom_type} = this.props;
        try {
            const {data} = await axios.delete('/custom-types/' + custom_type.name + '/' + deleteItem.name);
            this.props.dispatch({
                type: ACTION_REMOVE_FIELD, payload: data,
            })
        } catch (e) {
            console.error(e)
        } finally {
        }
    };


    hideDialog = () => {
        this.setState({
            showConfirmDialog: false,
        })
    }
}

const Styles = theme => ({
    table: {
        minWidth: 650,
    },
    emptyMain: {
        margin: theme.spacing(3),
    }
})
let Styled = withStyles(Styles)(FieldsTable);
export default connect()(Styled)