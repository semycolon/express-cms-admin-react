import React from 'react';
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import withStyles from "@material-ui/core/styles/withStyles";
import axios from 'axios';
import Paper from "@material-ui/core/Paper";
import {connect} from 'react-redux';
import {ADD_COLLECTION} from "../../../../../redux/actions/types";
import {Redirect} from "react-router-dom";
import FormHelperText from "@material-ui/core/FormHelperText";
import Card from "@material-ui/core/Card";
import {getBaseUrl} from "../../../../../lib/baseUrl";

class NewContentType extends React.Component {
    state = {
        name: '',
        label: '',
        errors: [],
        loading: false,
        redirect: false,
    }


    render() {
        const {name, label, errors, redirect,} = this.state;
        const {classes,} = this.props;

        const nameErrors = errors.filter(it => it.param === 'name')
        const labelErrors = errors.filter(it => it.param === 'label')
        return (
            <div className={classes.main}>
                <Card className={classes.card}>
                    <Typography className={classes.item} variant={"h6"}>
                        Create New Content Type
                    </Typography>
                    <TextField
                        className={classes.item}
                        fullWidth
                        label={"Display Name"}
                        onChange={(e) => this.setState({label: e.target.value})}
                        error={labelErrors.length > 0}
                        helperText={labelErrors ? labelErrors.map(it =>
                            <div>{it.msg ? it.msg : 'error'}</div>) : null}
                    />

                    <TextField
                        className={classes.item}
                        fullWidth
                        label={"Collection Name"}
                        onChange={(e) => this.setState({name: e.target.value})}
                        error={nameErrors.length > 0}
                        helperText={nameErrors.length ? nameErrors.map(it =>
                            <div>{it.msg ? it.msg : 'error'}</div>) : "Unique Identifier for Collection"}
                    />


                    <div style={{display: 'flex'}}>
                        <Button
                            onClick={this.handleSubmit}
                            className={`${classes.item} ${classes.submit_btn}`}
                            variant="contained" color={"primary"}
                            disabled={!name || !label}
                        >
                            Add Custom Type
                        </Button>
                    </div>
                </Card>
                {
                    redirect && <Redirect to={getBaseUrl() + "/dashboard/content-type-builder"}/>
                }
            </div>
        );
    }

    handleSubmit = async () => {
        const {name, label} = this.state;
        this.setState({
            loading: true,
            errors: [],
        })

        try {
            const {data} = await axios.post('/custom-types', {name, label});
            this.props.dispatch({type: ADD_COLLECTION, payload: data, newItem: {name, label}})
            this.setState({
                name: '',
                label: '',
                errors: [],
                loading: false,
                redirect: true,
            })
        } catch (e) {
            console.error(e)
            if (e.response && e.response.data && e.response.data.errors) {
                this.setState({
                    errors: e.response.data.errors,
                })
            }
        } finally {
            this.setState({
                loading: false,
            })
        }
    }
}

const Styles = (theme) => ({
    main: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        margin: theme.spacing(0, 0, 4),
    },
    item: {
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(1),
    },
    submit_btn: {
        marginTop: theme.spacing(2),
        margin: 'auto',
        minWidth: 180
    },
    card:{
        padding:theme.spacing(2),
        marginTop:theme.spacing(4)
    },
})

let NewCustomTypeWithStyles = withStyles(Styles)(NewContentType);
export default connect()(NewCustomTypeWithStyles);