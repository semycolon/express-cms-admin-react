import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';

import 'fontsource-roboto';
import './plugins/axios';
import {ThemeProvider} from '@material-ui/core/styles'
import createMuiTheme from "@material-ui/core/styles/createMuiTheme";
import {Provider as ReduxProvider} from 'react-redux';
import store from './redux/store.js'
import responsiveFontSizes from "@material-ui/core/styles/responsiveFontSizes";

console.log('base url is', window.__baseurl__);

let theme = createMuiTheme({
    palette: {
        type: 'dark',
    }
})

theme = responsiveFontSizes(theme);

ReactDOM.render(
    <React.StrictMode>
        <ReduxProvider store={store}>
            <ThemeProvider theme={theme}>
                <App/>
            </ThemeProvider>
        </ReduxProvider>
    </React.StrictMode>
    ,
    document.getElementById('root')
);
