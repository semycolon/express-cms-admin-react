function readJson(field) {
    const it = localStorage.getItem(field);
    try {
        return it && JSON.parse(it);
    } catch (e) {
        console.error(e);
        return null;
    }
}

function writeJson(field, data) {
    localStorage.setItem(field, JSON.stringify(data));
}

export default class Auth {
    static auth = readJson('auth');

    static logout() {
        localStorage.removeItem('token');
        localStorage.removeItem('user');
    }

    static isAuthenticated() {
        return localStorage.getItem('token') && readJson('user');
    }


    static setToken(token) {
        localStorage.setItem('token', token)
    }

    static setUser(user) {
        writeJson('user', user);
    }

    static getUser() {
        return readJson('user')
    }
}