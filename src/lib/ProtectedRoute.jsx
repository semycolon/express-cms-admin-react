import Auth from "./Auth";
import React from "react";
import {Route, Redirect,withRouter} from "react-router-dom";

function render(auth, Component) {
    return (props) => {
        if (!auth) {
            return <Component  {...props}/>;
        }
        if (auth) {
            let authenticated = Auth.isAuthenticated();
            if (authenticated) {
                return <Component {...props} />;
            } else {
                return <Redirect to={{pathname: "/login"}}/>
            }
        }
    };
}

export default function ProtectedRoute({auth, component: Component, path, ...rest}) {
  return <Route key={path} path={path}  {...rest} render={render(auth, Component)}
  />
}
