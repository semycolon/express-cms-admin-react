export function getBaseUrl(){
    if (devlopeMode()) {
        return '';
    }else{
        if (window.__baseUrl__) {
            return window.__baseUrl__
        }
        return '';
    }
}

function devlopeMode() {
    return !process.env.NODE_ENV || process.env.NODE_ENV === 'development'
}