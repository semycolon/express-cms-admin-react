import React, {Component} from 'react';
import axios from 'axios';

export default class WithAuth extends Component {
    state = {
        loading:true,
    }

    componentDidMount() {
        axios.get('auth/me')
            .then(({data}) => {
                console.log('/me data', data);
            })
            .finally(() =>{
                this.setState({
                    loading:false
                })
            })
    }

    render() {
        if (this.state.loading) {
            return (
                <div>Fetching Data from server ...</div>
            );
        } else {
            return (
                <React.Fragment>
                    {this.props.children}
                </React.Fragment>
            );
        }
    }
}