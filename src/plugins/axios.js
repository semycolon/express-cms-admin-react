import axios from 'axios';

axios.defaults.baseURL = window.__apiBaseUrl__ || process.env.REACT_APP_BASE_URL;
axios.defaults.headers.common['token'] = localStorage.getItem('token');