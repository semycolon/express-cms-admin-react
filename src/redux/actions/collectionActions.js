import axios from "axios";
import {FETCH_COLLECTIONS} from "./types";

export function fetchCollections() {
    return function (dispatch) {
        axios.get('/custom-types')
            .then(({data}) => {
                console.log('data', data);
                dispatch({
                    type: FETCH_COLLECTIONS,
                    payload: data,
                })
            })
            .catch(e => {
                console.error(e)
            })

    }
}