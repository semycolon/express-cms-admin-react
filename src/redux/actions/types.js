export const FETCH_COLLECTIONS = 'FETCH_COLLECTIONS'
export const ADD_COLLECTION = 'ADD_COLLECTION'
export const REMOVE_COLLECTION = 'REMOVE_COLLECTION'
export const COLLECTION_NEW_FIELD = 'COLLECTION_NEW_FIELD'
export const ACTION_REMOVE_FIELD = 'ACTION_REMOVE_FIELD';



export const ACTION_FETCH_CMS = 'ACTION_FETCH_CMS'
export const ACTION_CMS_ADDED_ITEM = 'ACTION_CMS_ADDED_ITEM'