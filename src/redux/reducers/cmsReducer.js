import {ACTION_CMS_ADDED_ITEM, ACTION_FETCH_CMS} from "../actions/types";

const initialState = {
    cms: {},
}
export default function (state = initialState, action) {
    switch (action.type) {
        case ACTION_FETCH_CMS : {
            const {collection_name, data} = action.payload;
            const cms = {...state.cms}
            cms[collection_name] = data;
            return {
                ...state,
                cms,
            }
        }
        case ACTION_CMS_ADDED_ITEM:{
            const {collection_name, data} = action.payload;
            const cms = {...state.cms}
            cms[collection_name].push(data);

            return {
                ...state,
                cms,
            }
        }
        default: {
            return state;
        }
    }
}