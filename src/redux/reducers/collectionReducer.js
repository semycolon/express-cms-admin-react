import {
    ACTION_REMOVE_FIELD,
    ADD_COLLECTION,
    COLLECTION_NEW_FIELD,
    FETCH_COLLECTIONS,
    REMOVE_COLLECTION
} from "../actions/types";

const initialState = {
    collections: [],
};

export default function (state = initialState, action) {
    switch (action.type) {
        case FETCH_COLLECTIONS: {
            return {
                ...state,
                collections: action.payload,
            }
        }
        case ADD_COLLECTION : {
            return {
                ...state,
                collections: action.payload,
            }
        }
        case REMOVE_COLLECTION : {
            return {
                ...state,
                collections: action.payload,
            }
        }
        case COLLECTION_NEW_FIELD: {
            const updatedItem = action.payload;
            let {collections} = state;
            for (const key in collections) {
                let it = collections[key];
                if (it._id === updatedItem._id) {
                    collections[key] = updatedItem;
                }
            }

            return {
                ...state,
                collections: [...collections]
            };
        }
        case ACTION_REMOVE_FIELD:{
            const updatedItem = action.payload;
            let {collections} = state;
            for (const key in collections) {
                let it = collections[key];
                if (it._id === updatedItem._id) {
                    collections[key] = updatedItem;
                }
            }
            return {
                ...state,
                collections: [...collections],
            }
        }
        default:
            return state;
    }
}