import {combineReducers} from "redux";
import CollecionReducers from './collectionReducer.js'
import cmsReducer from "./cmsReducer";

export default combineReducers({
    collections: CollecionReducers,
    cms: cmsReducer,
});
