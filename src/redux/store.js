import {createStore, compose, applyMiddleware} from 'redux';
import thunkMiddleware from 'redux-thunk'
import monitorReducerEnhancer from './enhancers/monitorReducer.js'
import loggerMiddleware from './middleware/logger.js'
import rootReducer from './reducers';

const middlewareEnhancer = applyMiddleware(/*loggerMiddleware,*/thunkMiddleware);
const composedEnhancers = compose(middlewareEnhancer, /*monitorReducerEnhancer*/window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())

const store = createStore(rootReducer, {}, composedEnhancers);

export default store;